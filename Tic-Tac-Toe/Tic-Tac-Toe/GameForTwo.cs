﻿using System;
using System.Collections.Generic;

namespace Tic_Tac_Toe
{
    internal class GameForTwo
    {
        internal static void Start(int numberOfRows, int howMuchToWin)
        {
            var board = new GameBoard();
            var fieldsList = new List<FieldInfo>();
            bool player1Turn = true;

            while (true)
            {
                Console.Clear();

                if(player1Turn)
                    Console.WriteLine("Turn: Player 1");
                else
                    Console.WriteLine("Turn: Player 2");
                
                fieldsList = board.DrawBoard(numberOfRows, fieldsList);

                var tmp = new FieldInfo
                {
                    Character = 'a'
                };

                tmp = CheckInput(fieldsList, tmp);

                foreach (var elem in fieldsList)
                {
                    if (elem.Name == tmp.Name)
                    {
                        if (player1Turn)
                            elem.Character = 'x';
                        else
                            elem.Character = 'o';
                    }
                }

                var checkForWin = new CheckForWin();

                if (checkForWin.IsThisWin(fieldsList, numberOfRows, howMuchToWin, player1Turn))
                {
                    Win.WinnigWindow(player1Turn);
                    break;
                }


                if (player1Turn)
                    player1Turn = false;
               else
                    player1Turn = true;
            }
        }

        private static FieldInfo CheckInput(List<FieldInfo> fieldsList, FieldInfo tmp)
        {
            while (true)
            {
                var moveField = Console.ReadLine();

                foreach (var elem in fieldsList)
                {
                    if (elem.Name == moveField)
                    {
                        tmp = elem;
                    }
                }

                if (tmp.Character == ' ')
                    break;

                Console.WriteLine(
                    "To pole jest zajete lub nie istnieje, podaj inne pole, " +
                    "lub poprawn format (litera plus liczba, np A1) ");
            }
            return tmp;
        }
    }
}