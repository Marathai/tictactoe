﻿using System;
using System.Collections.Generic;

namespace Tic_Tac_Toe
{
    public class GameBoard
    {
        public List<FieldInfo> DrawBoard(int numberOfRows, List<FieldInfo> fieldInfos)
        {
            Console.WriteLine("Player_1 => X   Player_2 => O");

            Console.WriteLine();
            int counter = 1;
            int cursorXPosition = Console.CursorLeft;
            int cursorYPosition = Console.CursorTop;
            char characters;


            for (int i = numberOfRows; i > 0; i--)
            {
                characters = (char)('0' + counter);
                DrawBoardElements(characters, cursorXPosition, cursorYPosition + i * 2);
                counter++;
            }

            for (int i = numberOfRows; i > 0; i--)
            {
                characters = (char)('A' + i - 1);
                DrawBoardElements(characters, cursorXPosition + i * 3, cursorYPosition);
            }

            counter = 0;
            int helpCounter = 0;

            for (int i = numberOfRows; i > 0; i--)
            {
                for (int j = 1; j < numberOfRows + 1; j++)
                {
                    characters = '|';
                    DrawBoardElements(characters, cursorXPosition - 1 + j * 3, cursorYPosition + i * 2);

                    if (fieldInfos.Count < numberOfRows * numberOfRows)
                    {
                        AddToList(fieldInfos, j, helpCounter);
                    }

                    characters = fieldInfos[counter].Character;
                    DrawBoardElements(characters, cursorXPosition + j * 3, cursorYPosition + i * 2);

                    characters = '_';
                    DrawBoardElements(characters, cursorXPosition + j * 3, cursorYPosition - 1 + i * 2);
                    counter++;
                }
                helpCounter++;
            }

            Console.CursorTop = cursorYPosition + numberOfRows * 3;
            Console.CursorLeft = cursorXPosition;

            return fieldInfos;
        }

        private static void AddToList(List<FieldInfo> fieldInfos, int j, int helpCounter)
        {
            var element = new FieldInfo
            {
                Character = ' ',
                LetterProperty = (char) ('A' + j - 1),
                NumberProperty = (char) ('1' + helpCounter),
            };

            element.Name = element.LetterProperty + element.NumberProperty.ToString();
            fieldInfos.Add(element);
        }

        private void DrawBoardElements(char character, int XPosition, int YPosition)
        {
            Console.SetCursorPosition(XPosition, YPosition);
            Console.Write(character);
        }

        public static FieldInfo GetFieldInfo(List<FieldInfo> fieldInfos, char LetterProperty, char NumberProperty)
        {
            foreach (var field in fieldInfos)
            {
                if (field.LetterProperty == LetterProperty && field.NumberProperty == NumberProperty)
                {
                    return field;
                }
            }
            return null;
        }
    }
}
