﻿using System;
using System.Collections.Generic;

namespace Tic_Tac_Toe
{
    public class CheckForWin
    {
        public bool IsThisWin(List<FieldInfo> boardFieldInfos, int numberOfRowes, int howMuchToWin, bool player1)
        {
            char Character = player1 ? 'x' : 'o';
            return IsPlayerWinHorizontal(boardFieldInfos, howMuchToWin, Character) ||
                IsPlayerWinVertical(boardFieldInfos,howMuchToWin,Character) ||
                IsPlayerWinDiagonalRight(boardFieldInfos, howMuchToWin, Character) ||
                IsPlayerWinDiagonalLeft(boardFieldInfos, howMuchToWin, Character);
        }

        public bool IsPlayerWinHorizontal(List<FieldInfo> boardFieldInfos, int howMuchToWin, char Character)
        {
            foreach (var startField in boardFieldInfos)
            {
                int matchesCount = 0;
                for (int i = 0; i < howMuchToWin; i++)
                {
                    var nextField = GameBoard.GetFieldInfo(
                        boardFieldInfos,
                        (char)(startField.LetterProperty + i),
                        (char)(startField.NumberProperty)
                    );
                    if (nextField == null)
                    {
                        break;
                    }

                    if (nextField.Character == Character)
                    {
                        matchesCount++;
                    }
                }
                if (matchesCount == howMuchToWin)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsPlayerWinVertical(List<FieldInfo> boardFieldInfos, int howMuchToWin, char Character)
        {
            foreach (var startField in boardFieldInfos)
            {
                int matchesCount = 0;
                for (int i = 0; i < howMuchToWin; i++)
                {
                    var nextField = GameBoard.GetFieldInfo(
                        boardFieldInfos,
                        (char)(startField.LetterProperty),
                        (char)(startField.NumberProperty + i)
                    );
                    if (nextField == null)
                    {
                        break;
                    }

                    if (nextField.Character == Character)
                    {
                        matchesCount++;
                    }
                }
                if (matchesCount == howMuchToWin)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsPlayerWinDiagonalRight(List<FieldInfo> boardFieldInfos, int howMuchToWin, char Character)
        {
            foreach (var startField in boardFieldInfos)
            {
                int matchesCount = 0;
                for (int i = 0; i < howMuchToWin; i++)
                {
                    var nextField = GameBoard.GetFieldInfo(
                        boardFieldInfos,
                        (char)(startField.LetterProperty + i),
                        (char)(startField.NumberProperty + i)
                    );
                    if (nextField == null)
                    {
                        break;
                    }

                    if (nextField.Character == Character)
                    {
                        matchesCount++;
                    }
                }
                if (matchesCount == howMuchToWin)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsPlayerWinDiagonalLeft(List<FieldInfo> boardFieldInfos, int howMuchToWin, char Character)
        {
            foreach (var startField in boardFieldInfos)
            {
                int matchesCount = 0;
                for (int i = 0; i < howMuchToWin; i++)
                {
                    var nextField = GameBoard.GetFieldInfo(
                        boardFieldInfos,
                        (char)(startField.LetterProperty - i),
                        (char)(startField.NumberProperty + i)
                    );
                    if (nextField == null)
                    {
                        break;
                    }

                    if (nextField.Character == Character)
                    {
                        matchesCount++;
                    }
                }
                if (matchesCount == howMuchToWin)
                {
                    return true;
                }
            }
            return false;
        }
    }
}