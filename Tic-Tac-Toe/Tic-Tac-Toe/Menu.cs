﻿using System;

namespace Tic_Tac_Toe
{
    public class Menu
    {
        public static string Options()
        {
            Console.Clear();
            string welcomeTxt = "Welcome to Tic-Tac-Toe";
            Console.SetCursorPosition((Console.WindowWidth - welcomeTxt.Length) / 2, Console.CursorTop+2);
            Console.WriteLine(welcomeTxt+"\n");
            Console.WriteLine("Game for two - press S");
            Console.WriteLine("Change game options - press O");
            Console.WriteLine("Exit - press E");
            var result = Console.ReadLine();
            return result;
        }
    }
}