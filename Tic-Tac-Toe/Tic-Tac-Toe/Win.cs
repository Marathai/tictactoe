﻿using System;

namespace Tic_Tac_Toe
{
    internal class Win
    {
        public static void WinnigWindow(bool player1Turn)
        {
            Console.Clear();

            if(player1Turn)
                Console.WriteLine("Player 1 won!!!");
            else
                Console.WriteLine("Player 2 won!!!");
            Console.ReadLine();
        }
    }
}