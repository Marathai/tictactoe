﻿using System;
using System.Runtime;

namespace Tic_Tac_Toe
{
    internal class ChangeGameOptions
    {
        public static int[] AskForNewOptions()
        {
            int[] tmp;
            tmp = new int[2];

            while (true)
            {
                Console.WriteLine("Insert new board size");

                while (true)
                {
                    try
                    {
                        tmp[0] = Int32.Parse(Console.ReadLine());
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Podaj liczbe");
                    }
                }

                Console.WriteLine("Insert how many elements win game");

                while (true)
                {
                    try
                    {
                        tmp[1] = Int32.Parse(Console.ReadLine());
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Podaj liczbe");
                    }
                }

                if (tmp[0] > 2 && tmp[0] < 11 && tmp[1] > 2 && tmp[1] < 11 && tmp[0] >= tmp[1])
                    break;
                Console.WriteLine("Podaj poprawne wartosci z zakresu <3,10>");
            }
            return tmp;
        }
    }
}