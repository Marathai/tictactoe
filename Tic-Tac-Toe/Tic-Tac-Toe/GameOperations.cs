﻿using System;
using System.ComponentModel.Design;
using System.Text;

namespace Tic_Tac_Toe
{
    public class GameOperations
    {
        public static void Run()
        {
            int[] settings = new int[2]
                {3,3};

            while (true)
            {
                var result = Menu.Options();

                if(result == "E")
                    break;
                if (result == "O")
                   settings = ChangeGameOptions.AskForNewOptions();
                if (result == "S")
                    GameForTwo.Start(settings[0], settings[1]);
            }
        }
    }
}