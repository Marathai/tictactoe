﻿namespace Tic_Tac_Toe
{
    public class FieldInfo
    {
        public char Character = ' ';
        public string Name;
        public char LetterProperty;
        public char NumberProperty;
    }
}